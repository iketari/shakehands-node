'use strict';

const xmlsoccer = require('./xmlsoccer');
const fastestlivescores = require('./fastestlivescores');
const debug = require('debug')(`index:receiver`);

let receivers = [fastestlivescores],
    receiversHash = {
        xmlsoccer: xmlsoccer,
        fastestlivescores: fastestlivescores
    };

module.exports = {

    getTournaments () {
        return Promise.all(receivers.map((reciver) => reciver.getTournaments()))
                .then(function (dataList) {
                    debug('receive all sources getTournaments');
                    let tournaments = [];
                    dataList.forEach(sourceItem => {
                        let source = sourceItem.source;
                        tournaments = tournaments.concat(sourceItem.data.map(tournament => {
                            tournament.source = source;
                            return tournament;
                        }));
                    });

                    return tournaments;
                }, data =>
                    {
                        debug('error on receive all sources getTournaments', data);
                        return data;
                    }
                );
    },

    getTournament (opts) {
        let receiver = receiversHash[opts.source];

        return new Promise(function(resolve, reject) {
            if (receiver) {
                debug('Found receiver', receiver.toString());
                receiver.getTournament({id: opts.id}).then(resolve);
            } else {
                debug('No receiver', opts.source);
                reject('No receiver');
            }
        });
    },

    getMatch (opts) {
        let receiver = receiversHash[opts.source];
        return new Promise(function(resolve, reject) {
            if (receiver) {
                debug('Found receiver');
                receiver.getMatch({id: opts.id}).then(resolve);
            } else {
                debug('No receiver', opts.source);
                reject('No receiver');
            }
        });
    },

    getLive (opts) {
        let receiver = receiversHash[opts.source];

        return new Promise(function(resolve, reject) {
            if (receiver && receiver.getLive) {
                receiver.getLive().then(resolve, debug);
            } else {
                debug('No receiver', opts.source);
                reject('No receiver');
            }
        });
    },

    getActive () {
        return receivers.map(reciver => reciver.toString());
    }
};

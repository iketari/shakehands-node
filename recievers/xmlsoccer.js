'use strict';
const RECEIVER_NAME = 'xmlsoccer';

const _ = require('lodash');
const Xmlsoccer = require('xmlsoccer');
const debug = require('debug')('xmlsoccer:receiver');
const cacheDecorator = require('../modules/cache');

const config = {
    key: 'LVWAMMGLRXFQSPNMSXRXVHSXLMQREWYVLHLXKSDMRJSEPCJLFB',
    demo: false
};

const client = new Xmlsoccer(config.key, config.demo);
const CachemanFile = require('cacheman-file');
const Cacheman = require('cacheman-promise');

const cache = new Cacheman({
    ttl: 300,
    engine: new CachemanFile({})
});

debug('try to connect');
client.connect().then(() => {
  return client.CheckApiKey();
}).then(res => debug(res));


// promisify needs methods
[
    'GetAllLeagues',
    'GetLiveScoreByLeague',
    'GetFixtureMatchByID',
    'GetLiveScore',
    'GetFixturesByDateIntervalAndLeague'
].forEach(method => client.promisify(method));

/**
 * Common data format
 * @param data
 * @returns {{source: string, data: *}}
 * @private
 */
function _template (data) {
    return {
        source: RECEIVER_NAME,
        data
    };
}

module.exports = {

    getTournaments: cacheDecorator(
        function () {
            return client.GetAllLeagues().then(
                data => {
                    debug('getTournaments receive data from API', data);
                    return _template(
                        data.League.map(league => {
                            let leagueObj = _.pick(league, ['Id', 'Name', 'Country']);
                            leagueObj.livescore = league.Livescore === 'Yes';

                            return leagueObj;
                        })
                    );

                },
                data => {
                    debug('error on receive data from API', data);
                }
            );
        },
        {
            ttl: 2592000,
            defaults: {},
            cacheParams: [],
            cacheKey: 'GetAllLeagues',
            debug
        }
    ),

    getTournament: cacheDecorator(
        function (params) {
            return client.GetFixturesByDateIntervalAndLeague({
                startDateString: params.from,
                endDateString: params.to,
                league: params.id
            }).then(data => {
                let sendData = [];

                debug('getTournament success data', data);

                if (data.Match) {
                    debug('getTournament data.Match', typeof data.Match);

                    if (Array.isArray(data.Match)) {
                        data.Match.forEach(matchData => {
                            sendData.push(matchData);
                        });
                    } else {
                        sendData.push(data.Match);
                    }
                } else {
                    debug('getTournament empty response');
                }

                return _template(sendData);
            }, data => {
                debug('getTournament error', data);
                throw new Error(data);
            });
        },
        {
            ttl: 400,
            defaults: () => {
                let to = new Date(),
                    from = new Date();

                to.setDate(to.getDate() + 7);
                from.setDate(from.getDate() - 1);

                [to, from].forEach(date => {
                    date.setHours(0);
                    date.setMinutes(0);
                    date.setSeconds(0);
                    date.setMilliseconds(0);
                });

                return  {
                    from: from.toISOString(),
                    to: to.toISOString()
                };
            },
            cacheParams: ['id', 'from', 'to'],
            cacheKey: 'GetFixturesByDateIntervalAndLeague',
            debug
        }
    ),

    getMatch: cacheDecorator(
        function (params) {
            return client.GetFixtureMatchByID({
                Id: params.id
            }).then(data => {
                return _template(data.Match);
            },data => {
                debug('error GetFixtureMatchByID', data);
                throw new Error(data);
            });
        },
        {
            ttl: 400,
            defaults: {},
            cacheParams: ['id'],
            cacheKey: 'GetFixtureMatchByID',
            debug
        }
    ),

    getLive: cacheDecorator(
        function () {
            return client.GetLiveScore()
                .then(data => {
                    debug('get livescores from API', data);
                    return _template(data);
                }, err => {
                    debug('error on fetch livescores', err);
                });
        },
        {
            ttl: 30,
            cacheParams: [],
            cacheKey: 'GetLiveScore',
            defaults: {},
            debug
        }
    ),


    toString () {
        return RECEIVER_NAME;
    }
};

'use strict';
const RECEIVER_NAME = 'fastestlivescores';

const cacheDecorator = require('../modules/cache');

const url = require('url');
const qs = require('querystring');
const nodeFetch = require('node-fetch');
const debug = require('debug')(`${RECEIVER_NAME}:receiver`);


const config = {
    key: 'c2a2e40a07b94d76b61abf41efbae4a1',
    endpoint: 'https://api.crowdscores.com/api/v1/'
};


/**
 * Common data format
 * @param data
 * @returns {{source: string, data: Object}}
 * @private
 */
function _template (data) {
    return {
        source: RECEIVER_NAME,
        data: data
    };
}

/**
 * fastestlivescores API call
 * @param resource
 * @param params
 * @returns {Promise}
 * @private
 */
function _get (resource, params) {
    let destinationUrl = url.resolve(config.endpoint, resource);
    let req;

    if (params) {
	    destinationUrl += '?' + qs.stringify(params);
    }

    req = nodeFetch(destinationUrl, {
            method: 'GET',
            headers: {
                'x-crowdscores-api-key': config.key
            }
        });

	debug('_get', resource, destinationUrl);

    return req.then(
        res => {
            debug('_get ok', resource, destinationUrl);
            if (res.ok) {
                return res.json().then(
                    (data) => {
                        debug('_get parse ok');
                        return data;
                    },
                    (err) => {
                        debug('_get parse error', RECEIVER_NAME, err);
                        return err;
                    }
                );
            } else {
	            throw new Error('API error');
            }
        },
        res => debug('_get error', RECEIVER_NAME, res.status, res.json())
    );
}

function dateRange (from, to) {
	let tomorrow = new Date(),
		yesterday = new Date();

	yesterday.setDate(yesterday.getDate() + from);
	tomorrow.setDate(tomorrow.getDate() + to);


	[tomorrow, yesterday].forEach(date => {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		date.setMilliseconds(0);
	});

	return  {
		from: yesterday.toISOString(),
		to: tomorrow.toISOString()
	};
}


module.exports = {
	getTournaments: cacheDecorator(
		function () {
			return _get('competitions')
				.then(apiData => {
					debug('get tournaments from API', apiData);
					return _template(apiData);
				}, err => {
					debug('error on fetch', err);
				});
		},
		{
			ttl: 86400,
			defaults: {},
			cacheParams: [],
			cacheKey: 'competitions',
			debug
		}
	),

	getTournament: cacheDecorator(
		function (params) {
			return _get('matches', {
				competition_id: params.id,
				from: params.from,
				to: params.to
			}).then(data => {
				debug('get tournament from API');
				return _template(data);
			}, function (data) {
				debug('error on fetch tournament', data);
			});
		},
		{
			ttl: 380,
			cacheParams: ['id'],
			defaults: dateRange(-1, 1),
			cacheKey: 'matches',
			debug
		}
	),

	getLive: cacheDecorator(
		function (params) {
			return _get(`matches`, params)
				.then(data => {
					debug('get matches from API');
					return _template(data);
				}, err => {
					debug('error on fetch matches', err);
				});
		},
		{
			ttl: 30 * 60,
			cacheParams: ['from', 'to'],
			cacheKey: 'matches',
			defaults: dateRange(0, 2),
			debug
		}
	),

	getMatch: cacheDecorator(
		function (params) {
			return _get(`matches/${params.id}`)
				.then(data => {
					debug('get match from API');
					return _template(data);
				}, err => {
					debug('error on fetch match', err);
				});
		},
		{
			ttl: 60,
			cacheKey: 'match',
			cacheParams: ['id'],
			defaults: {},
			debug
		}
	),


    toString () {
        return RECEIVER_NAME;
    }
};

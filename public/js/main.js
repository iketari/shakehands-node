define(function (require) {
	'use strict';

	require('jquery');

	require('bootstrap-loader');

	require('./../css/main.css');

	var Backbone = require('backbone');
	var Marionette = require('backbone.marionette');

	var Router = require('./router');

	var App = Marionette.Application.extend({
		initialize: function(options) {
			this.router = new Router;
			console.log('My container:', options.container);
		}
	});

	var app = new App({container: '#app'});

	app.start();
	Backbone.history.start();

});
define(function () {
	'use strict';

	require('match.xml');

	var NoChildrenView = require('./nochildren');

	return Backbone.Marionette.CollectionView.extend({
		isEmpty: function(collection) {
			return collection.length === 0;
		},
		emptyView: NoChildrenView,
		childView: Backbone.View.extend({
				events: {
					'click .js-toggle': 'toggleAdditional'
				},

				template: fest.match,

				render: function () {
					var html = this.template(this.model.toJSON());
					this.$el.html(html);
				},

				toggleAdditional: function (event) {
					event.preventDefault();

					this.$('.js-additional').toggleClass('hidden');
				}
			})
	});
});
define(function () {
	'use strict';

	require('tournaments.xml');
	var tournamentsCollection = require('./../collections/tournaments'),
		tournamentView = require('./tournament');

	var TournamentsView = Backbone.Marionette.CompositeView.extend({

		template: fest.tournaments,

		childViewContainer: '.js-tournaments',
		childView: tournamentView,
		collection: tournamentsCollection
	});


	return TournamentsView;

});
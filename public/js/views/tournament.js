define(function () {
	'use strict';

	require('tournament.xml');

	var TournamentView = Backbone.Marionette.View.extend({

		template: fest.tournament,

		render: function () {
			var html = this.template(this.model.toJSON());
			this.setElement(html);
		}
	});


	return TournamentView;

});
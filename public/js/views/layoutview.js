define(function () {
	"use strict";

	var TournamentsView = require('./tournaments');
	var MatchesView = require('./matchesCollection');
	var LiveView = require('./liveCollection');
	var MatchDetailsView = require('./matchDetails');
	var NavView = require('./nav');

	var tournamentsCollection = require('./../collections/tournaments');
	var liveCollection = require('./../collections/live');

	var matchDetailsModels = {
		xmlsoccer: require('./../models/detail/xmlsoccer'),
		fastestlivescores: require('./../models/detail/fastestlivescores')
	};

	require('main.xml');


	var AppLayoutView = Backbone.Marionette.LayoutView.extend({
		template: fest.main,

		views: {},

		regions: {
			navbar: '#navbar',
			content: '#tournaments',
			modal: '#modal'
		},

		onShowNavBar: function (type) {
			this.getRegion('navbar').show(new NavView({
				model: new Backbone.Model({
					type: type
				})
			}));
		},

		onShowMatchDetail: function (type, id) {
			var Model = matchDetailsModels[type],
				model = new Model(),
				view;

			model.set(model.idAttribute, id);
			model.once('sync', function () {
				view = new MatchDetailsView({
					model: model
				});
				this.getRegion('content').show(view);
			}.bind(this));

			model.fetch();
		},

		onShowTournamentsList: function () {
			this.getRegion('content').show(new TournamentsView());
		},

		onShowMatchesList: function (type, id) {
			if (tournamentsCollection.isEmpty()) {
				tournamentsCollection.once('sync', this._showMatchesList.bind(this, type, id));
			} else {
				this._showMatchesList(type, id);
			}

		},

		onShowLiveList: function () {
			new Promise(function (resolve) {
				liveCollection.fetch({
					success: resolve
				});
			}).then(function () {
				this.getRegion('content').show(new LiveView({
					collection: liveCollection
				}));
			}.bind(this));
		},

		_showMatchesList: function (type, id) {
			var model = tournamentsCollection.find(function (model) {
				return model.get('source') === type && model.id == id; // jshint ignore:line
			});

			if (model) {
				this.getRegion('content').show(new MatchesView({
					collection: model.getMatchesCollections()
				}));
			}

		}
	});

	return AppLayoutView;
});
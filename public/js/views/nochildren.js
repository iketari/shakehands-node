define(function () {
	'use strict';

	require('nochildren.xml');

	return Backbone.Marionette.ItemView.extend({
		template: fest.nochildren,

		render: function () {
			var html = this.template();
			this.$el.html(html);
		}

	});
});
define(function () {
	'use strict';

	require('navbar.xml');

	return Backbone.Marionette.View.extend({

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
		},

		template: fest.navbar
	});
});
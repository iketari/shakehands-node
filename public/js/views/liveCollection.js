define(function () {
	'use strict';

	require('live.xml');

	var NoChildrenView = require('./nochildren');

	var RowView = Backbone.Marionette.ItemView.extend({
		tagName: 'tr',
		template: fest.live,

		render: function () {
			var html = this.template({
					block: 'row',
					params: this.model.toJSON()
				});

			this.$el.html(html);
		}
	});


	return Backbone.Marionette.CompositeView.extend({

		emptyView: NoChildrenView,

		events: {
			'click .js-toggle': 'toggleAdditional'
		},

		childView: RowView,

		// specify a jQuery selector to put the itemView instances in to
		childViewContainer: '.js-live-matches',

		template: fest.live,

		toggleAdditional: function (event) {
			event.preventDefault();

			this.$('.js-additional').toggleClass('hidden');
		}
	});
});
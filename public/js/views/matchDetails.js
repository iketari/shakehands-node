define(function () {
	'use strict';

	require('matchDetails.xml');

	return Backbone.Marionette.View.extend({

		initialize: function () {
			this.model.on('change', this.render, this);
		},

		events: {
			'click .js-toggle': 'toggleAdditional',
			'click .js-reload': 'uiUpdate'
		},

		template: fest.matchDetails,

		render: function () {
			var html = this.template(this.model.toJSON());
			this.$el.html(html);
		},

		uiUpdate: function (event) {
			event.preventDefault();
			this.update();
		},

		update: function () {
			this.model.fetch();
		},

		toggleAdditional: function (event) {
			event.preventDefault();

			this.$('.js-additional').toggleClass('hidden');
		}
	});
});
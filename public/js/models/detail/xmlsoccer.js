define(function () {
	"use strict";

	var _ = require('underscore');

	return Backbone.Model.extend({
		type: 'xmlsoccer',
		urlRoot: '/api/details/xmlsoccer/',
		idAttribute: "Id",

		parse: function (data) {
			return data.data;
		},

		toJSON: function () {
			var result = _.clone(this.attributes);
			result.id = this.id;
			result.source = 'xmlsoccer';

			return result;
		}

	});
});
define(function () {
	"use strict";

	var _ = require('underscore');

	return Backbone.Model.extend({
		type: 'fastestlivescores',
		urlRoot: '/api/details/fastestlivescores/',
		idAttribute: "dbid",

		parse: function (data) {
			return data.data;
		},

		toJSON: function () {
			var result = _.clone(this.attributes);
			result.id = this.dbid;
			result.source = 'fastestlivescores';

			return result;
		}
	});
});
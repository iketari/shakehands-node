define(function () {
	"use strict";

	var _ = require('underscore');

	return Backbone.Model.extend({
		idAttribute: "Id",

		parse: function (data) {
			return data;
		},

		toJSON: function () {
			var result = _.clone(this.attributes);
			result.id = this.id;
			result.source = 'xmlsoccer';

			return result;
		}

	});
});
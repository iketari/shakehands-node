define(function () {
	"use strict";

	var _ = require('underscore');

	return Backbone.Model.extend({
		idAttribute: "dbid",

		parse: function (data) {
			data.type = 'fastestlivescores';
			return data;
		},

		toJSON: function () {
			var result = _.clone(this.attributes);
			result.id = this.dbid;
			result.source = 'fastestlivescores';

			return result;
		}
	});
});
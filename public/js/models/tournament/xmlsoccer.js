define(function () {
	"use strict";

	var _ = require('underscore'),
		MatchCollection = require('./../../collections/matches');

	return Backbone.Model.extend({
		type: 'xmlsoccer',
		urlRoot: '/api/tournaments/xmlsoccer/',
		idAttribute: "Id",

		parse: function (data) {
			data.name = data.Name;
			data.country = data.Country;

			return data;
		},

		toJSON: function () {
			var result = _.clone(this.attributes);
			result.id = this.id;

			return result;
		},

		getMatchesCollections: function () {
			var matches = new MatchCollection(null, {
				tournamentId: this.id,
				type: this.type
			});

			matches.fetch();
			this.collection.trigger('get:matches', matches);

			return matches;
		}



	});
});
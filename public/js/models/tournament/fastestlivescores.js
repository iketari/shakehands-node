define(function () {
	"use strict";

	var _ = require('underscore'),
		MatchCollection = require('./../../collections/matches');

	return Backbone.Model.extend({
		type: 'fastestlivescores',
		urlRoot: '/api/tournaments/fastestlivescores/',
		idAttribute: "dbid",

		parse: function (data) {
			data.country = data.region.name;
			data.regionGroup = data.region.regionGroup.name;
			data.logo = data.flagUrl;

			return data;
		},

		toJSON: function () {
			var result = _.clone(this.attributes);
			result.id = this.id;

			return result;
		},

		getMatchesCollections: function () {
			var matches = new MatchCollection(null, {
				tournamentId: this.id,
				type: this.type
			});

			matches.fetch();
			this.collection.trigger('get:matches', matches);

			return matches;
		}
	});
});
define(function () {
	"use strict";

	var LayoutView = require('./views/layoutview');

	var Controller = Backbone.Marionette.Object.extend({
		initialize: function() {
			var layout = new LayoutView();
			layout.render();
			this.options.layout = layout;

			layout.$el.appendTo('#app');
		},

		tournamentsList: function() {
			this.getOption('layout').triggerMethod('show:tournaments:list');
			this.getOption('layout').triggerMethod('show:nav:bar', 'tournaments');
		},

		matchesList: function (type, id) {
			this.getOption('layout').triggerMethod('show:matches:list', type, id);
			this.getOption('layout').triggerMethod('show:nav:bar', 'matches');
		},

		liveList: function () {
			this.getOption('layout').triggerMethod('show:live:list');
			this.getOption('layout').triggerMethod('show:nav:bar', 'live');
		},

		matchDetails: function (type, id) {
			this.getOption('layout').triggerMethod('show:match:detail', type, id);
			this.getOption('layout').triggerMethod('show:nav:bar', 'match');
		},

		navBar: function (view) {
			this.getOption('layout').triggerMethod('show:nav:bar');
		}
	});

	var Router = Backbone.Marionette.AppRouter.extend({
		appRoutes: {
			'live': 'liveList',
			'tournaments/:type/:id': 'matchesList',
			'details/:type/:id': 'matchDetails',
			'(/)' : 'tournamentsList'
		},

		controller: new Controller
	});

	return Router;

});
define(function () {
	"use strict";

	var __instance;

	var FastestlivescoresModel = require('../models/tournament/fastestlivescores'),
		XmlsoccerModel = require('../models/tournament/xmlsoccer');

	var Tournaments = Backbone.Collection.extend({
		url: '/api/tournaments',
		initialize: function () {
			this.fetch();
		},
		model: function (model, options) {
			switch(model.source) {
				case 'fastestlivescores':
					return new FastestlivescoresModel(model, options);
				case 'xmlsoccer':
					return new XmlsoccerModel(model, options);
			}
		}
	});

	if (!__instance) {
		__instance = new Tournaments();
	}

	return __instance;
});
define(function () {
	"use strict";

	var FastestlivescoresModel = require('../models/match/fastestlivescores'),
		XmlsoccerModel = require('../models/match/xmlsoccer');

	var Live = Backbone.Collection.extend({

		initialize: function (models, options) {
			this.options = options;
			this.url = '/api/live/'+ options.type +'/';
		},

		parse: function (data) {
			return data.data;
		},

		model: function (model, options) {
			switch(options.collection.options.type) {
				case 'fastestlivescores':
					return new FastestlivescoresModel(model, options);
				case 'xmlsoccer':
					return new XmlsoccerModel(model, options);
			}
		}
	});

	return new Live([], {type : 'fastestlivescores'});
});
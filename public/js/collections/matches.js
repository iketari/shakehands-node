define(function () {
	"use strict";

	var FastestlivescoresModel = require('../models/match/fastestlivescores'),
		XmlsoccerModel = require('../models/match/xmlsoccer');

	var Matches = Backbone.Collection.extend({

		initialize: function (models, options) {
			this.options = options;
			this.url = '/api/tournaments/'+ options.type +'/' + options.tournamentId;
		},

		parse: function (data) {
			return data.data;
		},

		model: function (model, options) {
			switch(options.collection.options.type) {
				case 'fastestlivescores':
					return new FastestlivescoresModel(model, options);
				case 'xmlsoccer':
					return new XmlsoccerModel(model, options);
			}
		}
	});

	return Matches;
});
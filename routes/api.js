'use strict';

const express = require('express');
const router = express.Router();
const receivers = require('../recievers');
const debug = require('debug')(`api:server`);

function sendReceiverError(res, data) {
    res.status(500).send(data || 'ERROR!');
}

function sendReceiverData(res, data) {
    res.status(200).send(data);
}

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'shakehands API' });
});

/* GET Active Providers List */
router.get('/providers', function(req, res) {
    res.status(200).json({
        active: receivers.getActive()
    });
});

/* GET tournaments */
router.get('/tournaments', function(req, res) {
    receivers.getTournaments()
        .then(function (data) {
            sendReceiverData(res, data);
        }, function (data) {
            sendReceiverError(res, data);
        });
});

/* GET one tournament by id and source */
router.get('/tournaments/:source/:id', function(req, res) {
    let id = req.params.id,
        source = req.params.source;

    debug('API tournament', source, id);
    receivers.getTournament({
        id,
        source
    }).then(
        data => {
            sendReceiverData(res, data);
        },
        data => {
            sendReceiverError(res, data);
        }
    );
});

/* GET one tournaments */
router.get('/match/:source/:id', function(req, res) {
    let id = req.params.id,
        source = req.params.source;

    receivers.getMatch({
        id,
        source
    }).then(
        data => {
            debug('match DONE', source, id, data);
            sendReceiverData(res, data);
        },
        data => {
            debug('match ERROR', source, id, data);
            sendReceiverError(res, data);
        }
    );
});

router.get('/live/:source/', function (req, res) {
	let source = req.params.source;

	receivers.getLive({source}).then(
		data => {
			debug('live done');
			sendReceiverData(res, data);
		},
		data => {
			debug('live fail');
			sendReceiverError(res, data);
		}
	);
});

router.get('/details/:source/:id', function (req, res) {
	let source = req.params.source,
		id = req.params.id;

	receivers.getMatch({source, id}).then(
			data => {
			debug('live done');
			sendReceiverData(res, data);
		},
			data => {
			debug('live fail');
			sendReceiverError(res, data);
		}
	);
});

module.exports = router;

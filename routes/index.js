'use strict';

var express = require('express');
var router = express.Router();
var reciver = require('../recievers');


/*SPA*/
router.get('/', function (req, res) {
    res.render('alldata.xml');
});


router.get('/xmlsoccer', function(req, res, next) {
    reciver.GetAllLeagues().then(function (data) {
        res.render('xmlsoccer.xml', {
            title: 'leagues',
            data: data
        });
    }, function (err) {
        console.log('error!!')
        res.send(err);
    });
});

router.get('/xmlsoccer/livescores/:id', function(req, res, next) {
    reciver.GetLiveScoreByLeague({
        league: req.params.id
    }).then(function (data) {
        res.render('xmlsoccer.xml', {
            title: 'livescores',
            data: data
        });
    }, function (err) {
        res.send(err);
    });
});

router.get('/xmlsoccer/livescore/:matchId', function(req, res, next) {
    reciver.GetFixtureMatchByID({
        Id: req.params.matchId
    }).then(function (data) {
        res.render('xmlsoccer.xml', {
            title: 'livescore',
            data: data
        });
    }, function (err) {
        res.send(err);
    });
});


module.exports = router;

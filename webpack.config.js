'use strict';
var path = require('path');
var BowerWebpackPlugin = require("bower-webpack-plugin");
var webpack = require("webpack");


module.exports = {
	cache: true,
	devtool: 'source-map',
	entry: {
		app: [
			__dirname + '/public/js/main.js'
		]
	},
	output: {
		path: path.join(__dirname, './public/build/'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{test: /\.css$/, loader: 'style!css'},
			{ test: /\.less$/, loader: "style!css!less!" },
			{ test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff" },
			{ test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff" },
			{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
			{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" },

			{ test: /\.xml/, loader: "fest-loader" }
		]
	},
	plugins: [
		new BowerWebpackPlugin({
			modulesDirectories: ['./public/packages'],
			manifestFiles:      'bower.json',
			includes:           /.*/,
			excludes:           [],
			searchResolveModulesDirectories: true
		}),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery",
			"window.Tether": 'tether'
		})
	],
	resolve: {
		// Absolute path that contains modules
		root: __dirname,

		// Directory names to be searched for modules
		modulesDirectories: ['public/js', 'public/packages', 'node_modules', 'public/templates']
	}

};
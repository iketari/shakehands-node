'use strict';

const CachemanFile = require('cacheman-file');
const Cacheman = require('cacheman-promise');

const cache = new Cacheman({
	ttl: 300,
	engine: new CachemanFile({})
});

/**
 * Decorator for cache API responses
 * @param api
 * @param ttl
 * @param defaults
 * @param cacheKey
 * @param cacheParams
 * @param debug
 * @returns {Function}
 */
function cacheDecorator (api, {ttl, defaults, cacheKey, cacheParams, debug}) {
	const name = cacheKey + cacheParams.join('');

	if (typeof defaults === 'function') {
		defaults = defaults();
	}

	debug('cacheDecorator init', name, ttl);

	return function (params) {
		let callKey = cacheKey;

		debug('cacheDecorator call with params', params);

		params = Object.assign(defaults, params);

		cacheParams = Object.keys(params).filter(param => cacheParams.includes(param));

		cacheParams.forEach(key => {
			callKey += params[key];
		});

		debug('cacheKey for ', name, callKey);

		return new Promise((resolve, reject) => {

			function fromApi (params) {
				return api(params)
					.then(apiData => {
						debug('get from API', params);

						cache.set(callKey, apiData, ttl);
						resolve(apiData);
					}, err => {
						debug('error on fetch', err);

						reject(err);
					});
			}

			cache.get(callKey).then(data => {

				debug('success on cache fetch', callKey);

				if (data) {
					debug(`${callKey} get from cache`);
					resolve(data);
				} else {
					return fromApi(params);
				}
			}, cacheErr => {
				debug('error on cache fetch', cacheErr);

				return fromApi(params);
			});

		});

	};
}

module.exports = cacheDecorator;